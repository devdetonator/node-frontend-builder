FROM node:argon
RUN npm install -g bower; \
    npm install -g grunt-cli; \
    npm install -g gulp-cli; \
    npm install -g grunt; \
    npm install -g gulp; \
    npm install -g yo; \
    npm install -g protractor; \
    webdriver-manager update; \
    npm install jasmine-reporters;

